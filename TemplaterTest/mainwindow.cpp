#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    FillTable(ui->table1,"Tab1");
    FillTable(ui->table2,"Tab2");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::FillTable(QTableWidget* tab,QString sName)
{
    for(int i = 0; i < 5;i++)
    {
        tab->insertColumn(i);
        tab->setHorizontalHeaderItem(i,new QTableWidgetItem(QString("name%1").arg(i+1)));
    }
    tab->insertRow(0);
    tab->insertRow(0);
    tab->insertRow(0);
    tab->insertRow(0);
    tab->insertRow(0);
    for(int i = 0; i < tab->rowCount();i++)
        for(int j = 0; j < tab->columnCount();j++)
            tab->setItem(i,j,new QTableWidgetItem(sName+" "+QString("Value%1:%2").arg(i+1).arg(j+1)));
}

void MainWindow::on_selectButton_clicked()
{
    sFile = QFileDialog::getOpenFileName(this,
            tr("Load"), "",
            tr("Template (*.xlsx);;All Files (*)"));
}

void MainWindow::on_runButton_clicked()
{

    QMap<QString,QVariant> mapParams;
    for(int i = 0; i < ui->tableParams->rowCount();i++)
        mapParams[ui->tableParams->item(i,0)->text().toLower()] = ui->tableParams->item(i,1)->text();

    QMap<QString,QAbstractItemModel*> mapTables;
    mapTables["Tab1"]=ui->table1->model();
    mapTables["Tab2"]=ui->table2->model();

    XlsxTemplate tmplt;
    tmplt.SetTemplate(sFile);
    tmplt.SetParams(mapParams);
    tmplt.SetTables(mapTables);

    if (!tmplt.Run("out.xlsx")) {
        qWarning() << tmplt.GetLastError();
    }
}
