#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "xlsxtemplate.h"

#include <QMainWindow>
#include <QTableWidget>
#include <QFileDialog>
#include <QAbstractTableModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_selectButton_clicked();

    void on_runButton_clicked();

private:
    Ui::MainWindow *ui;
    void FillTable(QTableWidget* tab,QString sName);
    QString sFile;
};

#endif // MAINWINDOW_H
