# Xlsx Template

Экспорт таблиц в xlsx-файлы

## Требования

 - С++17
 - модуль xlsx (https://github.com/VSRonin/QtXlsxWriter)

## Авторы

* **Andrey Trsv** - [trsv](https://gitlab.com/trsv)

## Лицензия

Этот проект лицензирован по лицензии Apache License 2.0 - подробности см. в файле LICENSE.


