#include "xlsxtemplate.h"

XlsxTemplate::XlsxTemplate()
{

}

XlsxTemplate::~XlsxTemplate()
{

}

void XlsxTemplate::SetTemplate(const QString &sFilename)
{
    m_templateFileName = sFilename;
}

void XlsxTemplate::SetParams(const QMap<QString, QVariant> &mapParams)
{
    m_params = mapParams;
}

void XlsxTemplate::SetTables(const QMap<QString, QAbstractItemModel *> &mapTables)
{
    m_tables = mapTables;

    m_columns.clear();

    // сохраняем название стобцов
    for (auto it = m_tables.cbegin(); it != m_tables.cend(); ++it) {
        for (int column = 0; column < it.value()->columnCount(); column++) {
            m_columns[it.key()] << it.value()->headerData(column, Qt::Horizontal).toString();
        }
    }
}

bool XlsxTemplate::Run(const QString &sNewFilename)
{
//    // копируем файл шаблона
//    if (!copyTemplate(sNewFilename)) {
//        return false;
//    }

    auto document = xlCreateXMLBook();

    if (!document) {
        m_lastError = QString("failed to create instance for %1").arg(sNewFilename);
        return false;
    }

    if (!document->load(m_templateFileName.toStdWString().c_str())) {
        m_lastError = QString("failed to load file %1").arg(sNewFilename);
        return false;
    }

    document->setKey(L"GCCG", L"windows-282123090cc0e6036db16b60a1o3p0h9");

    auto sheet = document->getSheet(0);

//    // поиск параметров
    const auto parameters = findParameters(sheet);

    // обрабатываем каждый параметр
    for (const auto & parameter : parameters) {
        if (!processParameter(parameter, sheet)) {
            return false;
        }
    }

    // поиск таблиц
    const auto tables = findTables(sheet);

    // собираем каждую найденую таблицу
    for (const auto & table : tables) {
        if (!processTable(table, sheet)) {
            return false;
        }
    }

    const auto isSaved = document->save(sNewFilename.toStdWString().c_str());
    document->release();

    if (!isSaved) {
        m_lastError = QString("failed to save document %1").arg(sNewFilename);
    }

    return isSaved;
}

QString XlsxTemplate::GetLastError() const
{
    return m_lastError;
}

bool XlsxTemplate::copyTemplate(const QString &path)
{
    QFile file(m_templateFileName);
    if (!file.exists()) {
        m_lastError = QString("template file %1 not exists").arg(m_templateFileName);
        return false;
    }

    if (!file.copy(path)) {
        m_lastError = QString("failed to copy template file %1 to output file %2")
                .arg(m_templateFileName, path);
        return false;
    }

    return true;
}

std::tuple<bool, QVariant> XlsxTemplate::getValueByParameter(const Parameter &parameter,
                                                          const QString & tableName,
                                                          const int &rowInTable)
{
    // каждый параметр обрабатываем согласно логике
    switch (parameter.type) {
    case Parameter::Type::BeginTable:
    case Parameter::Type::EndTable: {
        // начало и конец таблиц удаляем
        return {true, QString()};
    }
    case Parameter::Type::Param: {

        // параметр заменяем значением
        if (m_params.contains(parameter.name)) {
            return {true, m_params.value(parameter.name)};
        } else {
            return {false, QString("parameter with name %1 not found in parameters")
                        .arg(parameter.name)};
        }

    }
    case Parameter::Type::Value: {

        // берем из таблицы

        bool isColumnNumber = false;
        auto columnNumber = parameter.column.toInt(&isColumnNumber);

        // если id колонки - номер, берем значение из таблицы
        // иначе id колонки - название, вычисляем номер колонки
        if (!isColumnNumber) {
            columnNumber = m_columns[tableName].indexOf(parameter.column);
        }

        if (columnNumber >= 0) {
           return {true, m_tables[tableName]->index(
                               rowInTable, columnNumber).data()};
        } else {
            return {false, QString("column with name or number %1 not found")
                        .arg(parameter.column)};
        }
    }
    }

    return {false, QString("unknown parameter type")};
}

bool XlsxTemplate::processParameter(const XlsxTemplate::Parameter &parameter,
                                    libxl::Sheet * document)
{
    // значение параметра
    const auto [isSuccess, result] = getValueByParameter(parameter);

    // если параметр удачно обработался, то записываем результат в документ
    if (isSuccess) {

        const auto raw = document->readStr(parameter.rowFind, parameter.columnFind);

        if (raw) {
            const auto cellValue = QString::fromWCharArray(raw)
                    .replace("<@" + parameter.rawData + "@>", result.toString());

            document->writeStr(parameter.rowFind, parameter.columnFind, cellValue.toStdWString().c_str());
        }


    }

    return isSuccess;
}

bool XlsxTemplate::processTable(const Table & table, libxl::Sheet * document)
{
    // указанная таблица в шаблоне может не существовать
    if (!m_tables.contains(table.name)) {
        m_lastError = QString("table %1 not found").arg(table.name);
        return false;
    }

    auto lastRowForWrite = table.rowBegin + m_tableRowShift;

    const auto [firstRowInsert, lastRowInsert] = computeTableRange(table);

    if (!document->insertRow(firstRowInsert, lastRowInsert)) {
        m_lastError = QString("failed to insert row for range %1 %2")
                .arg(QString::number(firstRowInsert + 1), QString::number(lastRowInsert + 1));
        return false;
    }

//    return true;

    // проходимся по всем строкам в таблице
    for (int row = 0; row < m_tables[table.name]->rowCount(); row++, lastRowForWrite++) {

        // проходимся по каждой ячейке в строке таблицы и обрабатываем ее
        for (int iTemplate = 0, column = table.columnBegin;
             iTemplate < table.rowTemplate.size();
             iTemplate++, column++)
        {

            // проверяем
            if (column > table.columnEnd) {
                column = table.columnBegin;
                ++lastRowForWrite;
            }

            // текст ячейки, который будем обрабатывать
            auto cellValue = table.rowTemplate.at(iTemplate);

            // список параметров в текущей ячейке
            const auto parameters = Parameter::listFromRaw(row, column, cellValue);

            // обрабатываем каждый параметр
            for (const auto & parameter : parameters) {

                // значение параметра после обработки
                const auto [isSuccess, result] = getValueByParameter(parameter, table.name,
                        (table.isFixed) ? 0 : row);

                // если параметр обработать не удалось, возвращаем обшибку
                if (!isSuccess) {
                    m_lastError = result.toString();
                    return false;
                }

                // заменяем параметр на значение параметра
                cellValue = cellValue.replace("<@" + parameter.rawData + "@>", result.toString());
            }

            // записываем обработанную ячейку
            document->writeStr(lastRowForWrite, column, cellValue.toStdWString().c_str(),
                            table.rowFormatTemplate.at(iTemplate));
        }
    }

    return true;
}

QList <XlsxTemplate::Table> XlsxTemplate::findTables(libxl::Sheet * document)
{
    // поиск параметров типа table (begin/end)
    static const QRegularExpression tableFinder("<@type=\".*?table\".*?@>");

    const auto rowCount = document->lastRow();
    const auto columnCount = document->lastCol();

    // имя текущей таблицы
//    QString tableName;

    // найдено ли начало таблицы
    bool isBeginTableFinded = false;

    // параметр таблицы из ячейки
    auto getTableParameter = [](const QString & cell) -> std::tuple<bool, Parameter> {
        const auto parameters = Parameter::listFromRaw(0, 0, cell);

        for (const auto & parameter : parameters) {
            if (parameter.type == Parameter::Type::BeginTable ||
                    parameter.type == Parameter::Type::EndTable) {
                return {true, parameter};
            }
        }

        return {false, Parameter()};
    };

    QStack <Table> tables;
    for (int row = 0; row < rowCount; row++) {
        for (int column = 0; column < columnCount; column++) {

            // текущая ячейка в документе
            const auto raw = document->readStr(row, column);

            if (!raw) {
                continue;
            }

            const auto cell = QString::fromWCharArray(raw);

            // есть ли в ячейке начало/конец таблицы
            const auto isTableBeginOrEnd = tableFinder.match(cell).hasMatch();

            // если в ячейке есть начало/конец таблицы
            if (isTableBeginOrEnd) {

                // то начинаем/заканчиваем таблицу
                isBeginTableFinded = !isBeginTableFinded;

                // парсим параметр
                const auto [isFinded, parameter] = getTableParameter(cell);

                // если параметр спарсен удачно
                if (isFinded) {

                    // и параметр указывает на начало таблицы
                    if (isBeginTableFinded) {
                        // то инициализируем таблицу

                        Table table;

                        table.name = parameter.table;
                        table.rowBegin = row;
                        table.columnBegin = column;
                        table.isFixed = parameter.isReplace;

                        tables.push(table);
                    } else {
                        // иначе завершаем таблицу

                        auto & table = tables.top();
                        table.rowTemplate << cell;
                        table.rowFormatTemplate << document->cellFormat(row, column);
                        table.rowEnd = row;
                        table.columnEnd = column;
                        table.computeRowHeight();
                    }
                }
            }

            // если начало таблицы найдено, то собираем всю информацию о строке в таблице
            if (isBeginTableFinded) {
                auto & table = tables.top();
                table.rowTemplate << cell;
                table.rowFormatTemplate << document->cellFormat(row, column);
            }

        }
    }

    return tables.toList();
}

QHash<QString, XlsxTemplate::Parameter> XlsxTemplate::findParameters(libxl::Sheet * document)
{
    // поиск параметров типа param
    static const QRegularExpression parameterFinder("<@param=.*?@>");

    const auto rowCount = document->lastRow();
    const auto columnCount = document->lastCol();

    // список параметров типа param в заданой ячейке
    auto getParameters = [](const int & row, const int & column, const QString & cell) -> QList <Parameter> {
        const auto parametersInCell = Parameter::listFromRaw(row, column, cell);

        QList <Parameter> parameters;
        for (const auto & parameter : parametersInCell) {
            if (parameter.type == Parameter::Type::Param) {
                parameters << parameter;
            }
        }

        return parameters;
    };

    QHash <QString, Parameter> parameters;
    for (int row = 0; row < rowCount; row++) {
        for (int column = 0; column < columnCount; column++) {
            const auto raw = document->readStr(row, column);

            if (raw) {
                const auto cell = QString::fromStdWString(raw);
                const auto parametersInCell = getParameters(row, column, cell);

                for (const auto & parameter : parametersInCell) {
                    parameters.insert(parameter.name, parameter);
                }
            }



        }
    }

    return parameters;
}

std::tuple<int, int> XlsxTemplate::computeTableRange(const XlsxTemplate::Table &table)
{
    const auto tableHeight = m_tables[table.name]->rowCount() * table.rowHeight - table.rowHeight;

    const auto firstRowInsert = table.rowEnd + 1 + m_tableRowShift;
    const auto lastRowInsert = firstRowInsert + tableHeight - 1;

    m_tableRowShift += tableHeight;
    return {firstRowInsert, lastRowInsert};
}

XlsxTemplate::Parameter XlsxTemplate::Parameter::fromRaw(const int &row, const int &column,
                                                         const QString &raw)
{
    // парсим пару ключ=значение
    static const QRegularExpression parameterParser("([^=,]*)=(?|\"([^\"]*)\"|([^,\"]*))");

    Parameter parameter;

    parameter.rowFind = row;
    parameter.columnFind = column;
    parameter.rawData = raw;

    const auto rawArgs = raw.split(";");
    for (const auto & rawArg : rawArgs) {

        const auto arg = parameterParser.match(rawArg);
        const auto name = arg.captured(1).toLower();
        const auto value = arg.captured(2);

        if (name == "type") {

            if (value == "begintable") {
                parameter.type = Type::BeginTable;
            } else if (value == "endtable") {
                parameter.type = Type::EndTable;
            } else if (value == "value") {
                parameter.type = Type::Value;
            }

        } else if (name == "column") {
            parameter.column = value;
        } else if (name == "table") {
            parameter.table = value;
        } else if (name == "replace") {
            parameter.isReplace = value == "true";
        } else if (name == "param") {
            parameter.name = value;
            parameter.type = Type::Param;
        }

    }

    return parameter;
}

QList<XlsxTemplate::Parameter> XlsxTemplate::Parameter::listFromRaw(const int & row,
                                                                    const int & column,
                                                                    const QVariant &value)
{
    if (!value.isValid()) {
        return QList <Parameter> ();
    }

    // поиск параметров
    static const QRegularExpression parameterValidator("<@(.*?)@>");

    QList <Parameter> parameters;

    auto parameterMatchIterator = parameterValidator.globalMatch(value.toString());

    while (parameterMatchIterator.hasNext()) {
        const auto parameterMatch = parameterMatchIterator.next();
        const auto parameter = Parameter::fromRaw(row, column, parameterMatch.captured(1));

        parameters.append(parameter);
    }

    return parameters;
}

void XlsxTemplate::Table::computeRowHeight()
{
    rowHeight = rowEnd - rowBegin + 1;
}
