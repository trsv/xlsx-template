#ifndef XLSXTEMPLATE_H
#define XLSXTEMPLATE_H

#include "libxl.h"

#include <tuple>

#include <QtCore>

class XlsxTemplate
{
private:

    /**
     * @brief The Parameter struct информация о параметре <@...@>
     */
    struct Parameter {
        enum class Type {
            Param,
            Value,
            BeginTable,
            EndTable
        };

        // значение атрибута replace
        bool isReplace = false;

        // строка в документе, где найден параметр
        int rowFind = 0;

        // столбец в документе, где найден параметр
        int columnFind = 0;

        // тип параметра
        Type type;

        // название/номер столбца (для table value)
        QString column;

        // имя таблицы (для table)
        QString table;

        // имя параметра (для param)
        QString name;

        // исходный текст параметра
        QString rawData;

        static Parameter fromRaw(const int & row, const int & column, const QString & raw);
        static QList <Parameter> listFromRaw(const int &row, const int &column, const QVariant & name);
    };

    /**
     * @brief The Table struct информация о таблице <@begin/end table@>
     */
    struct Table {

        // начало таблицы в документе
        int rowBegin = 0;
        int rowEnd = 0;

        // конец таблицы в документе
        int columnBegin = 0;
        int columnEnd = 0;

        int rowHeight = 0;

        // имя таблицы
        QString name;

        // статическая или динамическая таблица
        bool isFixed = false;

        // шаблон строки
        QStringList rowTemplate;
        QList <libxl::Format*> rowFormatTemplate;

        void computeRowHeight();
    };

    /**
     * @brief m_templateFileName имя файла шаблона
     */
    QString m_templateFileName;

    /**
     * @brief m_lastError последняя ошибка
     */
    QString m_lastError;

    /**
     * @brief m_params список параметров
     */
    QMap<QString, QVariant> m_params;

    /**
     * @brief m_tables список таблиц
     */
    QMap<QString, QAbstractItemModel*> m_tables;

    /**
     * @brief m_columns список столбцов в таблицах
     */
    QHash <QString, QStringList> m_columns;

    /**
     * @brief m_tableRowShift сдвиг в документе по строкам
     */
    int m_tableRowShift = 0;

public:
    XlsxTemplate();
    ~XlsxTemplate();

    /*Задаёт путь к шаблону*/
    void SetTemplate(const QString & sFilename);

    /*Карта с параметрами*/
    void SetParams(const QMap<QString, QVariant> & mapParams);

    /*Карта с таблицми*/
    void SetTables(const QMap<QString,QAbstractItemModel*> & mapTables);

    bool Run(const QString & sNewFilename);

    QString GetLastError() const;

private:
    /**
     * @brief copyTemplate копирует файл шаблона по пути @path
     * @param path путь к новому файлу
     * @return успех копирования
     */
    bool copyTemplate(const QString & path);

    /**
     * @brief getValueByParameter получить значение параметра
     * @param parameter параметр
     * @param tableName имя таблицы, если обрабатываем параметр таблицы
     * @param rowInTable строка таблицы, если обрабатываем параметр таблицы
     * @return успех обработки и значение параметра
     */
    std::tuple<bool, QVariant> getValueByParameter(const Parameter & parameter,
                                                const QString &tableName = QString(),
                                                const int & rowInTable = 0);

    /**
     * @brief processParameter обработка параметра типа param
     * @param parameter параметр для обработки
     * @param document документ для записи результата
     * @return успех обработки
     */
    bool processParameter(const Parameter &parameter, libxl::Sheet *document);

    /**
     * @brief processTable создание таблицы
     * @param table параметры таблицы
     * @param document документ для записи результатов
     * @return успех операции
     */
    bool processTable(const Table &table, libxl::Sheet * document);

    /**
     * @brief findTables сбор информации о параметрах типа begin/end table
     * @param document документ для поиска
     * @return список таблиц для создания
     */
    QList<Table> findTables(libxl::Sheet *document);

    /**
     * @brief findParameters сбор информации о параметрах типа param
     * @param document документ для поиска
     * @return список параметров для обработки
     */
    QHash<QString, Parameter> findParameters(libxl::Sheet *document);

    std::tuple <int, int> computeTableRange(const Table & table);

};

#endif // XLSXTEMPLATE_H
