CONFIG += c++17

SOURCES += \
    $$PWD/xlsxtemplate.cpp

HEADERS += \
    $$PWD/xlsxtemplate.h

win32: LIBS += -L$$PWD/3rdparty/libxl/lib64/ -L$$PWD/3rdparty/libxl/bin64/ -llibxl

INCLUDEPATH += $$PWD $$PWD/3rdparty/libxl/include_cpp
DEPENDPATH += $$PWD $$PWD/3rdparty/libxl/include_cpp
